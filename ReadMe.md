分词评测工具说明
==============
使用streamlit做的一个web分词对比评测工具，集成了多个主流分词，可对其结果进行直观的对比评测。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1226/164813_4fb5d527_5038328.jpeg "ui.jpg")

安装
-------
**1. 海量分词**<br>
- 解压缩3rdUtiliy目录下的pyhlseg.zip	
- `python setup.py install`
- [海量分词项目地址](https://gitee.com/ryan70/pyhlseg)

**2. 百度aip**<br>
- 解压缩3rdUtiliy目录下的baidu-aip-python-sdk-2.2.5.zip	
- `python setup.py install`

**3.结巴分词**<br>
- `pip install jieba`

**4.LTP分词**<br>
- windows下遇到编译错误，无法安装，所以屏蔽了在SegmentAgent.py中的代码
- `pip install pyltp`

**5.HanLP分词**
- 已安装

**6.NLPIR分词**
- `pip install pynlpir`

如果你的NLPIR授权过期了，可以到[github的license地址下载](https://github.com/NLPIR-team/NLPIR/tree/master/License/license%20for%20a%20month/NLPIR-ICTCLAS%E5%88%86%E8%AF%8D%E7%B3%BB%E7%BB%9F%E6%8E%88%E6%9D%83)新的NLPIR.user，然后覆盖pynlpir的安装目录的DATA文件夹中的原文件(NLPIR.user)即可

**7.THULAC**
- `pip install thulac`

**8.streamlit**
- `pip install streamlit`

**8.jpype**
- `pip install jpype1>=0.7`

运行
----
- `streamlit run segEvalTool.py`
- 本地访问：http://localhost:8501/
- 注意：
	- 不要使用IE浏览器
	- 发现NLPIR和THULAC的分词结果中有丢字符的问题，导致和Hylanda分词比较时有时无法对齐比对
	- 如果系统安装了多个版本的jre，jpype.getDefaultJVMPath()返回的又不是正确的版本，则可以直接修改segmentAgent.py的load()函数的相应代码： 
		`jvmPath = jpype.getDefaultJVMPath()`
	 参考下面的代码指定正确的jvmPath：
	 linux: 
		`jvmPath = "/usr/local/jdk/jdk1.8.0_162/jre/lib/amd64/server/libjvm.so"`
	 Windows: 
		`jvmPath = 'C:\\Program Files\\Java\\jdk1.8.0_144\\jre\\bin\\server\\jvm.dll'`
