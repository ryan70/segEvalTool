#! /usr/bin/python
# -*-coding: UTF-8 -*-
#

import streamlit as st
from segmentAgent import *
from pyhlseg import *
from segResultCompare import *
from functools import reduce

g_md_font_nr_start = '<font size=3 color=#0000ff>'
g_md_font_ns_start = '<font size=3 color=#FF9600>'
g_md_font_nt_start = '<font size=3 color=#00BF00>'
g_md_font_t_start = '<font size=3 color=#4bb3fb>'
g_md_font_m_start = '<font size=3 color=#a00c21>'
g_md_font_q_start = '<font size=3 color=#c126fb>'
g_md_font_mq_start = '<font size=3 color=#c126fb>'
g_md_font_end = '</font>'
g_md_diff_start = '<font size=1 color=#808080>*['
g_md_diff_end = ']*</font>'
g_md_weight_start = '<font size=1 color=#808080>*['
g_md_weight_end = ']*</font>'
g_fmt_dict = {'nr': g_md_font_nr_start, 'ns': g_md_font_ns_start, 'nt': g_md_font_nt_start, 't': g_md_font_t_start, \
              'mq': g_md_font_mq_start, 'm': g_md_font_m_start, 'q': g_md_font_q_start}


def gen_seg_result_text(seg_words):
    words_with_fmt_str = []
    for x in seg_words:
        word = x[0] if x[0] != '\n' else '<br>'
        cur_fmt_str = get_fmt_str(x[1])
        if cur_fmt_str != '':
            words_with_fmt_str.append(cur_fmt_str + word + g_md_font_end)
        else:
            words_with_fmt_str.append(word)
    return ' '.join(words_with_fmt_str)


def gen_keywords_text(keywords):
    words_with_fmt_str = []
    for x in keywords:
        words_with_fmt_str.append(x.word_str)
        words_with_fmt_str.append(f'{g_md_weight_start}{x.weight:.4f}{g_md_weight_end}\t')
    return ' '.join(words_with_fmt_str)


def gen_diff_text(diff):
    is_diff = False
    results = []
    diff_item_count = diff.size()
    for i in range(diff_item_count):
        word, nature, diff_flag = diff.get_item(i)
        if word == '\n' and diff_flag != SegDifference.FLAG_DIFF_ANSWER:
            word = '<br>'
        if diff_flag == SegDifference.FLAG_DIFF_ANSWER:
            word = word.replace('\n', '\\n')
        if diff_flag == SegDifference.FLAG_DIFF_ANSWER:
            if not is_diff:
                cur_fmt_str = get_fmt_str(nature)
                if cur_fmt_str != '':
                    cur_fmt_str = cur_fmt_str.replace('size=3', 'size=1') + '*['
                else:
                    cur_fmt_str = g_md_diff_start
                results.append(cur_fmt_str)
                is_diff = True
        else:
            if is_diff:
                results.append(g_md_diff_end)
                is_diff = False
            cur_fmt_str = get_fmt_str(nature)
            if cur_fmt_str != '':
                word = cur_fmt_str + word + g_md_font_end
        results.append(word)
    if is_diff:
        results.append(g_md_diff_end)
    return ' '.join(results)


def get_fmt_str(pos):
    return g_fmt_dict[pos] if pos in g_fmt_dict else ''


def main():
    st.title("海量分词对比评测工具")
    input_text = st.text_area('测试文本', value='天津海量科技股份有限公司').strip()
    seg_engine = st.sidebar.radio('分词引擎', SegmentAgent.get_seg_engines())
    compare = st.sidebar.checkbox('与Hylanda分词结果进行比较')
    seg_button = st.empty()
    st.sidebar.subheader('Hylanda分词选项：')
    seg_grain_size = st.sidebar.selectbox('分词粒度', ('大', '普通', '小'))
    merge_mq_in_ng_mode = st.sidebar.checkbox('在普通颗粒下是否合并输出数量词', value=True)
    merge_dt_in_ng_mode = st.sidebar.checkbox('在普通颗粒下是否合并输出时间词')
    merge_book_in_ng_mode = st.sidebar.checkbox('在普通颗粒下是否合并输出书名')
    merge_org_in_ng_mode = st.sidebar.checkbox('在普通颗粒下是否合并机构名')
    merge_approximate_in_ng_mode = st.sidebar.checkbox('在普通颗粒下是否合并大约数')
    keyword_button = st.sidebar.empty()
    if len(input_text) > 0:
        grain_size = GrainSize.NORMAL if seg_grain_size == '普通' \
            else (GrainSize.LARGE if seg_grain_size == '大' else GrainSize.SMALL)
        SegHylanda.set_option(grain_size=grain_size, \
                              merge_org_in_n_grain_mode=merge_org_in_ng_mode, \
                              merge_mq_in_n_grain_mode=merge_mq_in_ng_mode, \
                              merge_date_time_words_in_n_grain_mode=merge_dt_in_ng_mode, \
                              merge_book_title_in_n_grain_mode=merge_book_in_ng_mode, \
                              merge_approximate_in_n_grain_mode=merge_approximate_in_ng_mode)
        if seg_button.button('分词'):
            seg_words = SegmentAgent.segment(input_text, seg_engine)
            if compare and seg_engine != 'Hylanda':
                seg_hylanda_words = SegmentAgent.segment(input_text, 'Hylanda')
                seg_len = reduce(lambda x, y: x + y, [len(x[0]) for x in seg_words], 0)
                seg_hylanda_len = reduce(lambda x, y: x + y, [len(x[0]) for x in seg_hylanda_words], 0)
                diff = SegResultCompare.compare(seg_hylanda_words, seg_words)
                if seg_len == seg_hylanda_len:
                    text_md = f'<font size=4 color=#ff0000>【说明：下面分词结果主体部分是海量分词结果，斜体字部分是对比分词结果】</font>)<br>' + gen_diff_text(
                        diff)
                else:
                    text_md = f'<font size=4 color=#ff0000>【比较结果长度不同】</font>: {seg_engine} - {seg_len}, Hylanda - {seg_hylanda_len}<br>' + gen_diff_text(
                        diff)
            else:
                text_md = gen_seg_result_text(seg_words)
            st.markdown(text_md, unsafe_allow_html=True)
            keyword_button.button('Hylanda分词关键词')
        elif keyword_button.button('Hylanda分词关键词'):
            keywords = SegHylanda.get_keywords(input_text)
            text_md = gen_keywords_text(keywords)
            st.markdown(text_md, unsafe_allow_html=True)


if __name__ == "__main__":
    main()
