#! /usr/bin/python
# -*-coding: UTF-8 -*-
#

class SegDifference:
    FLAG_SAME = 0
    FLAG_DIFF_STANDARD = 1
    FLAG_DIFF_ANSWER = 2

    def __init__(self):
        self._words = []
        self._natures = []
        self._diff_flags = []

    def add(self, word, nature, diff_flag):
        self._words.append(word)
        self._natures.append(nature)
        self._diff_flags.append(diff_flag)

    def size(self):
        return len(self._words)

    def get_item(self, index):
        return (self._words[index], self._natures[index], self._diff_flags[index])


class SegResultCompare:
    @staticmethod
    def compare(standard, answer):
        diff = SegDifference()
        answer_index = 0
        i = 0
        while i < len(standard):
            standard_word = standard[i]
            if answer_index >= len(answer):
                diff.add(standard_word[0], standard_word[1], SegDifference.FLAG_DIFF_STANDARD)
                i += 1
                continue

            answer_word = answer[answer_index]
            if len(standard_word[0]) == len(answer_word[0]):
                diff.add(standard_word[0], standard_word[1], SegDifference.FLAG_SAME)
                answer_index += 1
                i += 1
            else:
                diffLen = SegResultCompare.count_diff_length(standard, i, answer, answer_index)
                i += SegResultCompare.add_seg_result_to_diff_obj(diff, standard, i, diffLen, SegDifference.FLAG_DIFF_STANDARD)
                answer_index += SegResultCompare.add_seg_result_to_diff_obj(diff, answer, answer_index, diffLen, SegDifference.FLAG_DIFF_ANSWER)
        return diff

    @staticmethod
    def add_seg_result_to_diff_obj(diff, words, index, diff_len, diff_flag):
        standard_len = 0
        add_count = 0
        for i in range(index, len(words)):
            diff.add(words[i][0], words[i][1], diff_flag)
            add_count += 1
            standard_len += len(words[i][0])
            if standard_len == diff_len:
                break
        return add_count

    @staticmethod
    def count_diff_length(standard, stanard_index, answer, answer_index):
        standard_len = 0
        answer_len = 0
        while True:
            if standard_len > answer_len:
                if answer_index >= len(answer):
                    break
                answer_len += len(answer[answer_index][0])
                answer_index += 1
            elif standard_len < answer_len:
                standard_len += len(standard[stanard_index][0])
                stanard_index += 1
            else:
                if standard_len == 0:
                    answer_len += len(answer[answer_index][0])
                    answer_index += 1
                    standard_len += len(standard[stanard_index][0])
                    stanard_index += 1
                else:
                    break
        return standard_len
