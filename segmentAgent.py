#! /usr/bin/python
# -*-coding: UTF-8 -*-
#

import jieba.posseg as JiebaPosSeg
from aip import AipNlp as BaiduSeg
from pyhlseg import *
import jpype
from jpype import *
import threading
import os
import re
import pynlpir
import thulac
# from pyltp import Segmentor as LtpSeg
# from pyltp import Postagger	as LtpPOSTagger


class SegmentAgent:
    _lock = threading.RLock()
    _loaded = False
    _engines = {}
    @staticmethod
    def load():
        with SegmentAgent._lock:
            if not jpype.isJVMStarted():
                # 打开jvm虚拟机
                jar_path = os.path.abspath('./lib')
                word_seg_jar_path = os.path.join(jar_path, "word-1.3.1.jar")
                hanlp_seg_jar_path = os.path.join(jar_path, "hanlp-1.6.6.jar")
                hl_seg_jar = HylandaSegment.get_segment_jar_path()
                #如果系统安装了多个版本的jre，jpype.getDefaultJVMPath()返回的又不是正确的版本，则可以直接指定相应的版本
                #linux: jvmPath = "/usr/local/jdk/jdk1.8.0_162/jre/lib/amd64/server/libjvm.so"
                #Windows: jvmPath = 'C:\\Program Files\\Java\\jdk1.8.0_144\\jre\\bin\\server\\jvm.dll'
                jvmPath = jpype.getDefaultJVMPath()
                jpype.startJVM(jvmPath, "-ea",
                               f"-Djava.class.path={word_seg_jar_path};{hanlp_seg_jar_path};{hl_seg_jar}", \
                               f"-Djava.ext.dirs={jar_path}", \
                               convertStrings=False)

            if not SegmentAgent._loaded:
                SegmentAgent._init_segment_engine('Hylanda', SegHylanda)
                SegmentAgent._init_segment_engine('Baidu', SegBaidu)
                SegmentAgent._init_segment_engine('Jieba', SegJieba)
#                SegmentAgent._init_segment_engine('Word', SegWord)
                SegmentAgent._init_segment_engine('HanLP', SegHanLP)
                SegmentAgent._init_segment_engine('NlpIr', SegNlpIr)
                SegmentAgent._init_segment_engine('Thulac', SegThulac)
                print("initialize finished!\n server is running......")
                SegmentAgent._loaded = True

    @staticmethod
    def unload():
        with SegmentAgent._lock:
            for k in SegmentAgent._engines:
                SegmentAgent._engines[k].uninit()
            jpype.shutdownJVM()
            SegmentAgent._loaded = False

    @staticmethod
    def get_seg_engines():
        if not SegmentAgent._loaded:
            SegmentAgent.load()
        return list(SegmentAgent._engines.keys())

    @staticmethod
    def segment(text, engine_name):
        seg_words = SegmentAgent._engines[engine_name].segment(text)
        return seg_words

    @staticmethod
    def _init_segment_engine(engine_name, engine_class):
        try:
            engine_class.init()
            SegmentAgent._engines[engine_name] = engine_class
        except Exception as e:
            print(e)

class SegHylanda:
    __nature_dict__ = {}
    @staticmethod
    def init():
        HylandaSegment.init()
        HylandaSegment.load_dictionary()
        HylandaSegment.set_option(GrainSize.LARGE, merge_date_time_words_in_n_grain_mode=True, do_pos_tagging=True)
        SegHylanda.__nature_dict__ = {
            HylandaSegment.DictFlag.NATURE_ID_NR: 'nr', \
            HylandaSegment.DictFlag.NATURE_ID_NS: 'ns', \
            HylandaSegment.DictFlag.NATURE_ID_NT: 'nt', \
            HylandaSegment.DictFlag.NATURE_ID_T: 't', \
            HylandaSegment.DictFlag.NATURE_ID_M: 'm', \
            HylandaSegment.DictFlag.NATURE_ID_Q: 'q', \
            HylandaSegment.DictFlag.NATURE_ID_MQ: 'mq' \
            }
        print("Segmentation of Hylanda is init successfully")

    @staticmethod
    def uninit():
        HylandaSegment.uninit()

    @staticmethod
    def set_option(grain_size=GrainSize.NORMAL, merge_org_in_n_grain_mode=False,
                       merge_mq_in_n_grain_mode=False, merge_date_time_words_in_n_grain_mode=False,
                       merge_book_title_in_n_grain_mode=False, merge_approximate_in_n_grain_mode=False,
                       just_output_word_in_keywords=False, output_delimiter=True, output_stop_word=True,
                       do_pos_tagging=False):
            HylandaSegment.set_option(grain_size=grain_size, \
                                  merge_org_in_n_grain_mode=merge_org_in_n_grain_mode, \
                                  merge_mq_in_n_grain_mode=merge_mq_in_n_grain_mode, \
                                  merge_date_time_words_in_n_grain_mode=merge_date_time_words_in_n_grain_mode, \
                                  merge_book_title_in_n_grain_mode=merge_book_title_in_n_grain_mode, \
                                  merge_approximate_in_n_grain_mode=merge_approximate_in_n_grain_mode, \
                                  just_output_word_in_keywords=just_output_word_in_keywords, \
                                  output_delimiter=output_delimiter, \
                                  output_stop_word=output_stop_word, \
                                  do_pos_tagging=do_pos_tagging)

    @staticmethod
    def segment(text):
        results = []
        for word in HylandaSegment.seg_to_words(text):
            if word.nature in SegHylanda.__nature_dict__:
                results.append((word.word_str, SegHylanda.__nature_dict__[word.nature]))
            else:
                results.append((word.word_str, ''))
        return results

    @staticmethod
    def get_keywords(text):
        return HylandaSegment.get_keywords(text)


class SegJieba:
    @staticmethod
    def init():
        print("Segmentation of Jieba is init successfully")

    @staticmethod
    def uninit():
        pass

    @staticmethod
    def segment(text):
        return [(x.word, x.flag) for x in JiebaPosSeg.cut(text)]


class SegWord:
    _segmentor = None
    @staticmethod
    def init():
        if SegWord._segmentor is None:
            # 创建分词对象
            SegWord._segmentor = JPackage("org.apdplat.word").WordSegmenter
            SegWord._posTagging = JPackage("org.apdplat.word.tagging").PartOfSpeechTagging
            print("Segmentation of Word is init successfully")

    @staticmethod
    def uninit():
        SegWord._segmentor = None

    @staticmethod
    def segment(text):
        words = SegWord._segmentor.segWithStopWords(text)
        SegWord._posTagging.process(words)
        result = []
        for x in words:
            parts = re.split('/', str(x))
            result.append((parts[0], parts[1]))
        return result


class SegHanLP:
    _hanlp = None
    @staticmethod
    def init():
        if SegHanLP._hanlp is None:
            # 创建分词对象
            SegHanLP._hanlp = JPackage("com.hankcs.hanlp").HanLP
            print("Segmentation of HanLP is init successfully")

    @staticmethod
    def uninit():
        SegHanLP._hanlp = None

    @staticmethod
    def segment(text):
        return [(str(x.word), str(x.nature.toString())) for x in SegHanLP._hanlp.segment(text)]


class SegBaidu:
    _client = None
    @staticmethod
    def init():
        if SegBaidu._client is None:
            app_id = u"11064808"
            api_key = u"DQTrtth19CxxG0PVVMCxLMLz"
            secret_key = u"6Zz6ggEIg4Y7iLZZGSiHLAmPNVNe7suh"
            SegBaidu._client = BaiduSeg(app_id, api_key, secret_key)
            SegBaidu._client.setConnectionTimeoutInMillis(2000)
            SegBaidu._client.setSocketTimeoutInMillis(30000)
            print("Segmentation of Baidu is init successfully")

    @staticmethod
    def uninit():
        pass

    @staticmethod
    def segment(text):
        ret = []
        try:
            result = SegBaidu._client.lexer(text)
            words = result[u'items']
            for x in words:
                if len(x[u'ne']) > 0:
                    if x[u'ne'] == "PER": ret.append((x[u'item'], u'nr'))
                    if x[u'ne'] == "LOC": ret.append((x[u'item'], u'ns'))
                    if x[u'ne'] == "ORG": ret.append((x[u'item'], u'nt'))
                    if x[u'ne'] == "TIME": ret.append((x[u'item'], u't'))
                else:
                    ret.append((x[u'item'], x[u'pos']))
        except Exception as e:
            pass
        return ret


class SegNlpIr:
    @staticmethod
    def init():
        pynlpir.open()
        print("Segmentation of NlpIr is init successfully")

    @staticmethod
    def uninit():
        pynlpir.close()

    @staticmethod
    def segment(text):
        ret = []
        words = pynlpir.segment(text, pos_names='child',  pos_english=False)
        for x in words:
            if x[1] == '人名' or x[1] == '汉语姓氏' or x[1] == '汉语名字' or x[1] == '日语人名' or x[1] == '音译人名':
                ret.append((x[0], 'nr'))
            elif x[1] == '机构团体名':
                ret.append((x[0], 'nt'))
            elif x[1] == '地名' or x[1] == '音译地名':
                ret.append((x[0], 'ns'))
            elif x[1] == '时间词':
                ret.append((x[0], 't'))
            elif x[1] == '数词' or x[1] == '干支':
                ret.append((x[0], 'm'))
            elif x[1] == '量词' or x[1] == '动量词' or x[1] == '时量词':
                ret.append((x[0], 'q'))
            elif x[1] == '数量词':
                ret.append((x[0], 'mq'))
            else:
                ret.append(x)
        return ret


class SegThulac:
    _segmentor = thulac.thulac(seg_only=True)
    @staticmethod
    def init():
        print("Segmentation of Thulac is init successfully")

    @staticmethod
    def uninit():
        pass

    @staticmethod
    def segment(text):
        words = SegThulac._segmentor.cut(text, text=False)
        ret = []
        for x in words:
            if x[1] == 'np':
                ret.append((x[0], 'nr'))
            else:
                ret.append((x[0], x[1]))
        return ret


'''
class SegLtp(object):
	def __init__(self):
		self.__ltpSegmentor = LtpSeg()
		LTP_DATA_DIR = './ltp-data'
		cws_model_path = os.path.join(LTP_DATA_DIR, 'cws.model')
		self.__ltpSegmentor.load(cws_model_path)
		self.__ltpPOSTagger = LtpPOSTagger()
		pos_model_path = os.path.join(LTP_DATA_DIR, 'pos.model')
		self.__ltpPOSTagger.load(pos_model_path)
		words = self.segment(u'ltp分词加载测试')
		for x in words:	print x[0] + u'/' + x[1],
		print u'\n'
		pass

	def segment(self, text):
		words = self.__ltpSegmentor.segment(text.encode('utf-8'))
		postags = self.__ltpPOSTagger.postag(words)
		result = []
		for i in range(len(words)):
			result.append((words[i].decode('utf-8'), postags[i].decode('utf-8')))
		return result
'''

